# Nodejs web server for wireguard XP

A Web server built with express and Node.js. Supports SSl (listens to 80 and 443 ports) and scales across cores. Supports range requests streaming and chunked transfer encoding except chunk_size can't be changed from the `64KB` default.

## Installation & Usage

`npm i`

`node index.js`

For each request, payload file is served as response. Incase of streaming the payload file is streamed till the end of file.
