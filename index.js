const express = require("express");
const fs = require("fs");
const https = require("https");
const http = require("http");
const cluster = require("cluster");

if (cluster.isMaster) {
  // Creates the Forks
  process.title = "node-server";
  const { length: numberOfProc } = require("os").cpus();
  for (let i = 0; i < numberOfProc; i++) {
    cluster.fork();
  }

  // and here you can fork again when one of the forks dies
  cluster.on("exit", (worker, code, signal) => {
    console.error(
      `worker ${worker.process.pid} died (${
        signal || code
      }). restarting it in a sec`
    );
    setTimeout(() => cluster.fork(), 1000);
  });
} else {
  const app = express();
  app.all("/", (req, res) => {
    const filepath = `${__dirname}/payload`;
    // check if the file exists
    if (fs.existsSync(filepath)) {
      // Set headers for the download response
      const fileSize = fs.statSync(filepath).size;
      // Handle range requests for resuming downloads
      res.set({
        "Content-Type": "application/octet-stream",
        "Content-Length": fileSize,
        "Content-Disposition": `attachment; id="payload"`,
        "Cache-Control": "public, max-age=31536000",
      });

      const range = req.headers.range;
      if (range) {
        console.log("range: ", range);
        const parts = range.replace(/bytes=/, "").split("-");
        const start = parseInt(parts[0], 10);
        console.log("start: ", start);
        const end = parts[1] ? parseInt(parts[1], 10) : fileSize - 1;
        console.log("end: ", end);
        const chunkSize = end - start + 1;
        res.writeHead(206, {
          "Content-Type": "application/octet-stream",
          "Content-Range": `bytes ${start}-${end}/${fileSize}`,
          "Content-Length": chunkSize,
        });
        const file = fs.createReadStream(filepath, { start, end });
        let downloadedBytes = 0;
        file.on("data", function (chunk) {
          downloadedBytes += chunk.length;
          res.write(chunk);
        });
        file.on("end", function () {
          console.log("Download completed");
          res.end();
        });
        file.on("error", function (err) {
          console.log("Error while downloading file:", err);
          res.status(500).send("Error while downloading file");
        });
      } else {
        // Handle full file download requests
        const file = fs.createReadStream(filepath, {
          highWaterMark: 1024 * 1024,
        });
        file.pipe(res);
      }
    } else {
      res.status(204).send({}); // Send empty response
    }
  });

  // Listen both http & https ports
  const httpServer = http.createServer(app);
  const httpsServer = https.createServer(
    {
      key: fs.readFileSync("private.key"),
      cert: fs.readFileSync("public.crt"),
    },
    app
  );

  httpServer.listen(80, () => {
    console.log("HTTP Server running on port 80");
  });

  httpsServer.listen(443, () => {
    console.log("HTTPS Server running on port 443");
  });
}
